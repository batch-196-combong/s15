// Javascript Statements
	/*
		>>Set of instructions that we tell the computer to perform
		>>JS Statements usually ends with semicolon (;), to locate where the statements end

	*/
console.log ("Hello Once More");
// console.log is used to print in the console

// online comment (ctrl + /)
/* multi line comment (ctrl + shift + /)
*/

// Variables
/*
	Sytntax:
		let / const variableName;
*/

//initialize variables
let hello;
console.log(hello); // result: undefined

/*console.log(x);
let x;*/

// declaration of variables
	// Best practices:
let firstName = "Jin" //good variable name

let pokemon = 25000; // bad variable name

let FirstName = "Izuku"; //bad variable name
letFirstNameAgain = "All Might"; //good variable name -- camel case

// let first name = "Midoriya"; bad variable name because of space. 
// best to use is camelCase and underscore

let product_description = "lorem Ipsum";
let product_id = "250000ea1000";

/*
	Syntax:
	let / const variableName =value;
*/

let productName = "Desktop computer"
console.log(productName); //result

let prodPrice = 696969;
console.log (prodPrice);

const pi = 3.14;
console.log (pi);

// Reasigning variable
productName = "Laptop";
console.log (productName);

// const asignment can't be reassigned

// initialize variable
let supplier;
supplier = "Jane Smith";

// reassignment
supplier = "Zuitt"
console.log (supplier)

// var vs let/const

a = 5;
console.log (a)


b=6;
console.log(b)

let outerVariable ="hello"

{
	let innerVariable ="hello again"

}
console.log(outerVariable);

let productCode = "DC017", productBrand = "Dell";
console.log (productCode, productBrand);

/*const let = "hello"
console.log(let)*/

//Data Types
// string
let country = "Philippines"
let city = "Manila City"

console.log (city, country)

//concatenating strings
let fullAddress = city+", "+country
console.log(fullAddress)

let myName = "David";
let greeting =  "Hi, I am " + myName;
console.log (greeting)

// escape character
let mailAddress = "Metro Manila\n\nPhilippines"
console.log (mailAddress)

let message = 'John\'s employees went home early'

//numbers
// integers / whole numbers
let headcount = 26;
console.log (headcount);

// decimal numbers
let grade = 74.9;
console.log (grade);

let planetDistance = 2e10;
console.log(planetDistance);

// boolean
let isMarried = false;
let isSingle = true;
console.log ("isMarried " + isMarried);

// boolean declaration usually starsts with isName

// arrays
/*
	syntax:
	let/const arrayName = [];

*/

let grades = [98.7, 77.3, 90.8, 88.4];
console.log (grades)

let anime = ["BNHA", "AOT", "SxF", "KNY"]
console.log(anime)

// object data type
/*
	Syntax:
	let/const objectName = {
		propertyA: valueA,
		propertyB: valueB
	}
*/

let person = {
	fullName:"Edward Scissorhands",
	age: 35,
	isMarried: false,
	contact: ["09123456789", "8123 456"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person)

// type of operators
console.log (typeof person)

anime [0] = "One Punch Man"
console.log (anime)

// null
let spouse = null;
console.log (spouse)

// undefined
let y;
console.log (y);